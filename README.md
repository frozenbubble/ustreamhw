This repository contains the code for the server in this assignment.

# Build

### Requirements
This project requires jdk version 1.8 and gradle version 3.x to build

### Steps
To build the project you need to complete two steps.
* `cd` into src/main/resources/public and hit `npm install`
* `cd` into the project root and hit `gradle build`

### Docker
Once the build is complete, to build a docker image from the project hit
`docker image build -t ustreamhw:latest .`

When running the image you'll need to expose port 4567 in order to be able to access the server.
E.g. `docker container run -d -p 4567:4567 ustreamhw:latest `

# Architecture

This project is a barebones web server built with Spark. It has two web services that clients can access through http
* TimeService is used by the client to get the current time
* HistoryService is used by the web front end to query historic data. History service utilizes websockets in order to be able to notify the web front end of new requests.

# Front end

The displayes the requests of the past 5 minutes in a couple of forms. Once the server is running (e.g on port 4567) you can access it simply on http://localhost:4567/