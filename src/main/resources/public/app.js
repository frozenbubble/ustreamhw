"use strict"

let renderTimeline = (seq) => {
    let timeline = document.getElementById("timeline");
    timeline.innerHTML="";
    let width = timeline.clientWidth;
    let start = (new Date()).getTime();

    seq.forEach(entry => {
        let color = intToRGB(hashCode(entry.id))
        let offset = width * ((start - entry.time.getTime()) / 300000);
        let div = document.createElement("div");
        div.style.marginLeft = `${offset}px`;
        div.style.backgroundColor = `#${color}`;
        div.className = "timeline-dot";

        timeline.innerHTML += div.outerHTML;
    });
};

let getChartDrawer = (width, groupCounts, parent, type) => {
    return () => {
        let data = new google.visualization.DataTable();
        data.addColumn("string", "Client");
        data.addColumn("number", "Count");
        data.addRows(groupCounts);

        let options = {
            title: "Requests per client",
            width: width,
            height: width
        };

        let chart = undefined;
        switch (type) {
            case "bar":
                chart = new google.visualization.BarChart(parent);
                break;
            case "pie":
                chart = new google.visualization.PieChart(parent);
                break;
        }

        chart.draw(data, options);
    }
}

function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
} 

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}

let renderCharts = seq => {
    let pie = document.getElementById("piechart");
    let bar = document.getElementById("barchart");
    let dataset = _.chain(seq).groupBy("id").value();
    let groupCounts = Object.keys(dataset)
        .map(k => [k, dataset[k].length]);
    let width = 0.95 * document.getElementById("piechart-panel").clientWidth;

    console.log(groupCounts);

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(getChartDrawer(width, groupCounts, pie, "pie"));
    google.charts.setOnLoadCallback(getChartDrawer(width, groupCounts, bar, "bar"));

}

let renderTable = seq => {
    let table = document.getElementById("data-table");
    let t = document.createElement("table");
    t.className = "table"
    t.innerHTML += `
        <tr>
            <th>client</th>
            <th>time</th>
        </tr>`;

    seq.forEach(entry => {
        let time = `${entry.time.getHours()}:${entry.time.getMinutes()}:${entry.time.getSeconds()}`
        t.innerHTML += `<tr>
                <td>${entry.id}</td>
                <td>${time}</td>
            </tr>`
    });

    table.innerHTML = t.outerHTML;
}

let getLast5Mins = seq => {
    let result = seq.filter(entry => (new Date()).getTime() - entry.time.getTime() < 300000);
    
    // to mitigate wrong time coming from container
    if (result.length === 0) {
        return seq.splice(Math.max(0, (seq.length - 50)));
    }

    return result;
}

let onnewentry = msg => {
    let entry = JSON.parse(msg.data);
    history.push({
        id: entry.id.split("-")[0],
        addr: entry.addr,
        time: new Date(entry.time)
    });
    history = getLast5Mins(history);

    renderTimeline(history);
    renderTable(history);
    renderCharts(history);
}

let getHistory = () => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', '/api/history');
        xhr.onload = () => {
            resolve(JSON.parse(xhr.response).history);
        };
        xhr.onerror = function () {
            reject("xhr error");
        };
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        xhr.send();
    });
};

let history = [];
getHistory().then(
    h => {
        let mappedResults = h.map(e => {
            return {
                id: e.id.split("-")[0],
                addr: e.addr,
                time: new Date(e.time)
            };
        });

        history = getLast5Mins(mappedResults);

        renderTimeline(history);
        renderTable(history);
        renderCharts(history);
    },
    err => console.log(err)
);

let ws = new WebSocket(`ws://${location.hostname}:${location.port}/history`);
ws.onmessage = onnewentry;