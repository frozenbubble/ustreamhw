package ustreamhw.server.services;

import java.time.LocalDateTime;
import spark.Request;
import spark.Response;
import ustreamhw.protos.TimeServiceProtos.TimeResponse;

public class TimeService {
    public byte[] getTime(Request req, Response res) throws Exception {
        String id = req.params(":id");
        TimeResponse response = TimeResponse.newBuilder()
            .setTime(LocalDateTime.now().toString())
            .build();

        HistoryService.broadcastMessage(req.ip(), response.getTime(), id);

        return response.toByteArray();
    }
}