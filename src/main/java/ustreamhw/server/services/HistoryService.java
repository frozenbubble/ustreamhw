package ustreamhw.server.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.eclipse.jetty.websocket.api.*;
import java.util.logging.Logger;
import org.json.*;

import spark.Request;
import spark.Response;

@WebSocket
public class HistoryService {
    private static List<JSONObject> history = new ArrayList<>();
    private static List<Session> sessions = new ArrayList<>();
    private static final Logger log = Logger.getLogger(HistoryService.class.getName());

    public static void broadcastMessage(String addr, String timestamp, String id) {
        sessions.stream().filter(Session::isOpen).forEach(session -> {
            try {
                JSONObject historyEntry = (new JSONObject()
                    .put("addr", addr)
                    .put("time", timestamp)
                    .put("id", id)
                );

                history.add(historyEntry);
                session.getRemote().sendString(String.valueOf(historyEntry));
            } catch (Exception e) {
                log.severe("Error while broadcasting message. Reason: " + e.getMessage());
            }
        });
    }

    @OnWebSocketConnect
    public void onConnect(Session user) {
        log.info("User connected");

        sessions.add(user);
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        log.info("User left");

        sessions.remove(user);
    }

    public static String getHistory(Request req, Response res) {
        return String.valueOf(new JSONObject()
            .put("history", history)
        );
    }

    // @OnWebSocketMessage
    // public void onMessage(Session user, string)
}