package ustreamhw.server;

import static spark.Spark.*;
import ustreamhw.server.services.*;

public class Main {
    public static void main(String[] args) {
        TimeService service = new TimeService();

        webSocket("/history", HistoryService.class);
        staticFiles.location("/public");

        path("/api", () -> {
            before((req, res) -> {
                res.type("application/json");
            });

            get("/time/:id", service::getTime);
            get("/history", HistoryService::getHistory);
        });
        
        init();

        // internalServerError((req, res) -> {
        //     res.type("application/json");
        //     return "{\"error\":\"Unexpected error.\"}";
        // });
    }
}