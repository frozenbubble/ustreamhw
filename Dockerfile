FROM ubuntu:16.04

RUN apt-get update && apt-get install -y openjdk-8* && mkdir /app

COPY build /app

EXPOSE 4567

CMD java -jar /app/libs/ustreamhw.jar